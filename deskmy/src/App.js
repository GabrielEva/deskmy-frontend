import React from 'react';
import './App.css';

import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'

import { Layout } from 'antd';
import { Divider } from 'antd';
import { Avatar } from 'antd';
import { Typography } from 'antd';
import { Menu, Dropdown } from 'antd';

import { DownOutlined } from '@ant-design/icons';
import { UserOutlined } from '@ant-design/icons';

const { Title, Text } = Typography;
const { Header, Sider, Content } = Layout;

const menu = (
  <Menu>
    <Menu.Item key="0">
      <p>1st menu item</p>
    </Menu.Item>
    <Menu.Item key="1">
      <p>2nd menu item</p>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);


function App() {
  return (
    <div className="App">
      <Layout>
        <Header>Header</Header>
        <Layout>
          <Sider>
            <Avatar size={64} icon={<UserOutlined />} />
            <Title level={5}> Laís Galvão </Title>
            {/* Encapsular com Link? */}
            {/* <Link href="https://ant.design" target="_blank"> </Link> */}
            <div>
              <Text> UX Designer </Text>
            </div>
            
            <Divider />
            <Divider />
            <Divider />
          </Sider>
          <Content>Content</Content>
        </Layout>
      </Layout>
    </div>
  );
}

export default App;
